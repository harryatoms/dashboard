/**
 * Application Entry Point
 */
module Application {
    // template cache
    angular.module('templates', []);

    // routing set-up
    function routing($urlRouterProvider: ng.ui.IUrlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    }

    // theme
    function configureThemes($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('cyan')
            .accentPalette('blue-grey');

        $mdThemingProvider.theme('pink')
            .primaryPalette('pink')
            .accentPalette('blue-grey');
    }

    // application module
    var dependencies : string[] = [
        'ngMaterial',
        'ngMessages',
        'ngAnimate',
        'ui.router',
        'uiGmapgoogle-maps',
        'templates',
        'Sidebar',
        'PageHeader',
        'HomePage',
        'StoreManager'
    ];

    angular
        .module('Application', dependencies)
        .config(['$urlRouterProvider', routing])
        .config(['$mdThemingProvider', configureThemes]);
}