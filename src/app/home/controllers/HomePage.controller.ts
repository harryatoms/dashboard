module HomePage {
    'use strict';

    export class HomePageController{
        static $inject = ['$location'];
        constructor(private $location : ng.ILocationService) {}
    }
}