/**
 * Home Page Application
 */
module HomePage {

    /**
     * Routing
     */
    function routing($stateProvider: ng.ui.IStateProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'home/partials/index.tpl.html',
                controller: HomePageController,
                controllerAs: 'vm'
            })
    }

    angular
        .module('HomePage', ['ui.router'])
        .controller('HomePage.HomePageController', HomePageController)
        .config(['$stateProvider', routing]);
}