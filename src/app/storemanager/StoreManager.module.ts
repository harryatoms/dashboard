module StoreManager {

    /**
     * Routing
     */
    function routing($stateProvider: ng.ui.IStateProvider) {
        $stateProvider
            .state('storemanager-index', { // store listing
                url: '/storemanager/',
                templateUrl: 'storemanager/partials/index.tpl.html',
                controller: IndexController,
                controllerAs: 'vm'
            })
            .state('storemanager-editor-index', { // store editor
                url: '/storemanager/:store_id',
                templateUrl: 'storemanager/partials/store.tpl.html',
                controller: StoreController,
                controllerAs: 'vm'
            })
    }

    angular
        .module('StoreManager', ['ui.router'])
        .controller('StoreManager.IndexController', IndexController)
        .controller('StoreManager.StoreController', StoreController)
        .directive('storeSidebar', () => new StoreSidebar())
        .config(['$stateProvider', routing]);
}