module StoreManager {
    'use strict';
    import StatefulViewController = State.StatefulViewController;

    export enum EditorState {
        INFORMATION,
        LOCATIONS,
        MENUS
    }

    export class StoreController extends StatefulViewController{
        static $inject = ['$stateParams', '$location'];
        public locations : any[] = [];
        public name : string;
        public editorState : State.FSM<EditorState>;
        public EditorState = EditorState;
        public map;
        public events;
        constructor($stateParams, private $location : ng.ILocationService) {
            super();
            this.name = $stateParams.store_id;
            this.map = {center: {latitude: 31.572548, longitude: -84.150837}, zoom:12};
            this.events = {"click": this.mapClicked.bind(this)};
        }

        setUpStates() {
            var fsm = this.stateMachineFactory();
            this.editorState = new fsm<EditorState>(EditorState.INFORMATION);
            this.editorState.fromAny(EditorState).toAny(EditorState);
        }

        changeEditorState(state : EditorState) {
            this.editorState.go(state);
        }

        mapClicked(event) {
            console.log('cliq');
            console.log(event);
        }
    }
}

//protected setUpStates() {
//    var fsm = this.stateMachineFactory();
//    this.sidebarState = new fsm<SideBarState>(SideBarState.Collapsed);
//    this.sidebarState.from(SideBarState.Expanded).to(SideBarState.Collapsed);
//    this.sidebarState.from(SideBarState.Collapsed).to(SideBarState.Expanded);
//    this.sidebarState.onAny(this.onSideBarTransition.bind(this));
//}