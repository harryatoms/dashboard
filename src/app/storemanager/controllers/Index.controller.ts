module StoreManager {
    'use strict';

    import StatefulViewController = State.StatefulViewController;

    enum ListState {
        DEFAULT,
        SEARCH
    }

    export class IndexController extends StatefulViewController{
        static $inject = ['$location', '$timeout'];
        public locations : any[] = [];
        public ListState = ListState;
        public listState : State.FSM<ListState>;
        private locNames;


        constructor(private $location : ng.ILocationService, $timeout) {
            super();
            this.locNames = ['Zaxbys', 'Pearlys', 'McDonalds', 'Sonnys', 'Chikfila', 'Burger King', 'Steak and Shake', 'Applebees', 'LongHorn Steakhouse'];
            $timeout(function() {
                for (var i=0;i<=8;i++) {
                    var isActive = (i % 3 == 0);
                    this.locations.push({active:isActive, name:this.locNames[i]});
                }
            }.bind(this), 150);
        }

        protected setUpStates() {
            var fsm = this.stateMachineFactory();
            this.listState = new fsm<ListState>(ListState.DEFAULT);
            this.listState.from(ListState.DEFAULT).to(ListState.SEARCH);
            this.listState.from(ListState.SEARCH).to(ListState.DEFAULT);
        }

        remove(location) {
            this.locations.splice(this.locations.indexOf(location), 1);
        }

        addLocation() {
            var randomName = this.locNames[Math.floor(Math.random() * this.locNames.length)];
            this.locations.push({active:false, name:randomName})
        }

        toggleSearch() {
            console.log(this);
            var nextState : ListState = (this.listState.currentState == ListState.DEFAULT) ? ListState.SEARCH : ListState.DEFAULT;
            this.listState.go(nextState);
        }

        selectLocation(location) {
            this.$location.path('/storemanager/' + location.name);
        }
    }
}