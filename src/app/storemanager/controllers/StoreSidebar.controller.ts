module StoreManager {
    'use strict';

    export class StoreSidebarController{
        static $inject = ['$location'];
        public storeController : StoreController;
        public EditorState = EditorState;
        constructor(private $location : ng.ILocationService) {}

        public go(state : EditorState) {
            this.storeController.changeEditorState(state);
        }

        public currentState() {
            return this.storeController.editorState.currentState;
        }
    }
}