module StoreManager {
    export class StoreSidebar implements ng.IDirective {
        public restrict : string = 'E';
        public replace  : boolean = false;
        public templateUrl : string = 'storemanager/partials/store_sidebar.tpl.html';
        public controller = StoreSidebarController;
        public controllerAs : string = 'vm';
        public bindToController : boolean = true;
        public scope = {
            storeController: '='
        }
    }
}