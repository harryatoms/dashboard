module SideBar {

    export class DashboardSidebar implements ng.IDirective {
        public restrict : string = 'E';
        public replace  : boolean = true;
        public templateUrl : string = 'shared/sidebar/partials/sidebar.tpl.html';
        public controller = DashboardSidebarController;
        public controllerAs : string = 'vm';
    }


}