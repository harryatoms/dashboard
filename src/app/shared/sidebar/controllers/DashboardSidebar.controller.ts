module SideBar {
    import StatefulViewController = State.StatefulViewController;

    export enum SideBarState {
        Expanded,
        Collapsed
    }

    export class DashboardSidebarController extends StatefulViewController{
        static $inject = ['$location', '$mdSidenav'];
        public isPinned : boolean = true;
        public isCollapsed : boolean = false;
        public sidebarState : State.FSM<SideBarState>;
        public SideBarState = SideBarState;

        public constructor(private $location : ng.ILocationService, private $mdSidenav) {
            super();
            this.isCollapsed = true;
        }

        protected setUpStates() {
            var fsm = this.stateMachineFactory();
            this.sidebarState = new fsm<SideBarState>(SideBarState.Collapsed);
            this.sidebarState.from(SideBarState.Expanded).to(SideBarState.Collapsed);
            this.sidebarState.from(SideBarState.Collapsed).to(SideBarState.Expanded);
            this.sidebarState.onAny(this.onSideBarTransition.bind(this));
        }

        public toggle() {
            var nextState = !this.isCollapsed ? SideBarState.Collapsed : SideBarState.Expanded;
            this.sidebarState.go(nextState);
        }

        public onSideBarTransition(from: SideBarState, to:SideBarState) {
            this.isCollapsed = (to == SideBarState.Collapsed);
        }

        public navigateTo(path: string) {
            this.$location.path(path);
        }
    }
}