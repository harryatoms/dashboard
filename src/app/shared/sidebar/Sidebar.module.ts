module SideBar {
    angular.module('Sidebar', [])
        .directive('dashboardSidebar', () => new DashboardSidebar());
}