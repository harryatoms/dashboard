module PageHeader {
    export class BackButton implements ng.IDirective {
        public restrict : string = 'E';
        public replace  : boolean = false;
        public templateUrl : string = 'shared/pageheader/partials/backbutton.tpl.html';
        public controller = BackButtonController;
        public controllerAs : string = 'vm';
    }
}