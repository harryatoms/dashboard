module PageHeader {
    export class PageHeader implements ng.IDirective {
        public restrict : string = 'E';
        public replace  : boolean = false;
        public transclude : boolean = true;
        public templateUrl : string = 'shared/pageheader/partials/header.tpl.html';
    }
}