module PageHeader {
    angular.module('PageHeader', [])
        .directive('pageheader', () => new PageHeader())
        .directive('backbutton', () => new BackButton());
}