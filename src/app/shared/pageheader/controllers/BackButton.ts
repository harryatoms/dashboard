module PageHeader {
    export class BackButtonController {
        static $inject = ['$window'];
        public constructor(private $window) {}

        public back() {
            this.$window.history.back();
        }
    }
}