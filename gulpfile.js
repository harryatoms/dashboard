var gulp = require('gulp');
var browserSync = require('browser-sync');
var typescript = require('gulp-typescript');
var minifyCss = require('gulp-minify-css');
var tplCache = require('gulp-angular-templatecache');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var addStream = require('add-stream');
var mergeStream = require('merge-stream');
var runSequence = require('run-sequence');
var less = require('gulp-less');
var del = require('del');

/** Config */
var config = {
    buildDir: './dist',
    srcDir: './src',
    appDir:  './src/app',
    assetDir: './src/assets',
    publicAssets:'./src/assets/public'
};

/** default task */
gulp.task('default', function(done) {
    runSequence(
        'clean',
        ['copy', 'compile_stylesheets', 'compile_application', 'compile_libraries'],
        'serve',
        'watch',
        done
    );
});

/** Delete build directory */
gulp.task('clean', function() {
    return del(config.buildDir);
});

/** Serve build directory to browsers */
gulp.task('serve', function() {
    return browserSync({server: {baseDir: config.buildDir}});
});

/** Copy static assets to build directory */
gulp.task('copy', function() {
    var appEntry = gulp.src(config.srcDir + '/index.html')
        .pipe(gulp.dest(config.buildDir));

    var assets = gulp.src(config.publicAssets + '/**/*')
        .pipe(gulp.dest(config.buildDir));

    return mergeStream(appEntry, assets);
});

/** Compile Stylesheets */
gulp.task('compile_stylesheets', function() {return CompileStylesheets(); });

/** Compile Application */
gulp.task('compile_application', function() {
    // compile typescript
    return gulp.src([
            config.srcDir + '/tools/**/*.d.ts',
            config.assetDir + '/lib/fsm/**/*.ts',
            config.appDir + '/shared/**/*.ts',
            config.appDir + '/**/*.ts',
        ])
        .pipe(typescript({
            removeComments: true
        }))
        .pipe(addStream.obj(CompileTemplates()))
        .pipe(concat('Application.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.buildDir));
});

/** Compile 3rd Party Libraries */
gulp.task('compile_libraries', function() {
    // concat 3rd party scripts
    return gulp.src([
            config.assetDir + '/lib/typestate/dist/typestate.js',
            config.assetDir + '/lib/lodash/lodash.js',
            config.assetDir + '/lib/angular/angular.js',
            config.assetDir + '/lib/angular-aria/angular-aria.js',
            config.assetDir + '/lib/angular-animate/angular-animate.js',
            config.assetDir + '/lib/angular-messages/angular-messages.js',
            config.assetDir + '/lib/angular-material/angular-material.js',
            config.assetDir + '/lib/angular-touch/angular-touch.js',
            config.assetDir + '/lib/angular-carousel/dist/angular-carousel.js',
            config.assetDir + '/lib/angular-simple-logger/dist/angular-simple-logger.js',
            config.assetDir + '/lib/angular-google-maps/dist/angular-google-maps.js',
            config.assetDir + '/lib/ng-debounce/angular-debounce.js',
            config.assetDir + '/lib/angular-ui-router/release/angular-ui-router.min.js'
        ])
        .pipe(concat('lib.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.buildDir));
});

gulp.task('watch', function() {
    gulp.watch(config.appDir + '/**/*.ts', ['refresh_application']);
    gulp.watch(config.appDir + '/*.ts', ['refresh_application']);
    gulp.watch(config.appDir + '/**/*.tpl.html', ['refresh_application']);
    gulp.watch(config.appDir + '/*.tpl.html', ['refresh_application']);

    gulp.watch(config.appDir + '/**/*.less', ['refresh_stylesheets']);
    gulp.watch(config.appDir + '/*.less', ['refresh_stylesheets']);
    gulp.watch(config.appDir + '/**/*.css', ['refresh_stylesheets']);
    gulp.watch(config.appDir + '/*.css', ['refresh_stylesheets']);

    gulp.watch(config.assetDir + '/lib/*', ['refresh_libraries']);

    gulp.watch(config.publicAssets + '/**/*', ['refresh_assets']);
    gulp.watch(config.publicAssets + '/*', ['refresh_assets']);

    gulp.watch(config.srcDir + '/index.html', ['refresh_assets']);
    gulp.watch(config.appDir + '/**/*.css', ['refresh_stylesheets']);
});


/** Browser-Sync Helper Tasks */
gulp.task('refresh_application', ['compile_application'], browserSync.reload);
gulp.task('refresh_libraries', ['compile_libraries'], browserSync.reload);
gulp.task('refresh_assets', ['copy'], browserSync.reload);
gulp.task('refresh_stylesheets', function() {
    return CompileStylesheets().pipe(browserSync.stream());
});

/** Template Cache Helper */
function CompileTemplates() {
    return gulp.src(config.appDir + '/**/*.tpl.html')
        .pipe(tplCache());
}

/** Stylesheet Compiler Stream */
function CompileStylesheets() {
    var css = gulp.src([
        config.assetDir + '/lib/angular-material/angular-material.css',
        config.appDir + '/**/*.css'
    ]);

    var less_compiled = gulp.src([
        config.appDir + '/**/*.less'
    ])
        .pipe(less());

    return mergeStream(css, less_compiled)
        .pipe(concat('style.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest(config.buildDir));
}